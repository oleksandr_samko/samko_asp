﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Lab01.Controllers
{
    public class FeedbackController : Controller
    {
        [BindProperty]
        public MessageFeedback Feedback { get; set; }

        public IActionResult Index()
        {
            Feedback = new MessageFeedback();
            return View(Feedback);
        }

        public IActionResult RegisterFeedback()
        {
            Feedback.Name = Request.Form["Name"];
            Feedback.Email = Request.Form["Email"];
            Feedback.Message = Request.Form["Message"];
            return View(Feedback);
        }
        
    }
}