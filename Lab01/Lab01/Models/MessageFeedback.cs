﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Lab01
{
    public class MessageFeedback
    {
        [Required, StringLength(10)]
        public string Name;

        [Required, EmailAddress, Display(Name = "Емайл адрес")]
        public string Email;

        public string Message;

    }
}
